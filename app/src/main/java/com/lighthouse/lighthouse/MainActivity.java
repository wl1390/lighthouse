package com.lighthouse.lighthouse;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lighthouse.lighthouse.screen.LighthouseFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        LighthouseFragment targetFragment = new LighthouseFragment();
        replaceFragment(targetFragment);
    }


    public void replaceFragment(Fragment targetFragment) {
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        ft.addToBackStack("MainActivity");
        ft.replace(R.id.fragmentFrame, targetFragment, targetFragment.getTag()).commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0){
            getSupportFragmentManager().popBackStack();
        }
        else {
            super.onBackPressed();
            this.finishAffinity();
        }
    }
}
