package com.lighthouse.lighthouse.network;


import com.lighthouse.lighthouse.model.Response;

import retrofit2.http.GET;
import rx.Observable;

public interface RetrofitInterface {

    @GET("/lit")
    Observable<Response> lit();

    @GET("/off")
    Observable<Response> off();


}
