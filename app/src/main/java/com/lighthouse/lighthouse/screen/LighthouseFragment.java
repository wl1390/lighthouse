package com.lighthouse.lighthouse.screen;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.lighthouse.lighthouse.R;
import com.lighthouse.lighthouse.model.Response;
import com.lighthouse.lighthouse.network.NetworkUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class LighthouseFragment extends Fragment {

    private CompositeSubscription mSubscriptions;

    @BindView(R.id.textview_lighthouse)
    TextView lighthouse;

    @BindView(R.id.button_lit)
    Button lit;

    @OnClick(R.id.button_lit)
    public void clickLitButton(){

        mSubscriptions.add(NetworkUtil.getRetrofit()
                .lit()
                .observeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseLit, this::handleErrorLit));
    }

    @OnClick(R.id.button_off)
    public void clickOffButton(){
        mSubscriptions.add(NetworkUtil.getRetrofit()
                .off()
                .observeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseLit, this::handleErrorLit));
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lighthouse, container, false);
        ButterKnife.bind(this,view);
        return view;
    }


    private void handleResponseLit(Response response){
        Log.d("response","lighthouse "+response.getMessage()+" succeed");
        lighthouse.setText(response.getMessage());
    }

    private void handleErrorLit(Throwable error){
        Log.e("error", "Lighthouse lit fail: " + error.getMessage());
    }



}
